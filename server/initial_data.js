'use strict';

const countries = [
    { code: 'es', name: 'España'},
    { code: 'fr', name: 'Francia'},
    { code: 'pt', name: 'Portugal'},
    { code: 'de', name: 'Alemania'},
    { code: 'it', name: 'Italia'},
];


const cities = [
    { id: 1, name: 'Madrid', country_code: 'es', population: 6700000},
    { id: 2, name: 'Sevilla', country_code: 'es', population: 700000},
    { id: 3, name: 'Barcelona', country_code: 'es', population: 4800000},
    { id: 4, name: 'Málaga', country_code: 'es', population: 570000},
    { id: 5, name: 'Valencia', country_code: 'es', population: 800000},
    { id: 6, name: 'Badajoz', country_code: 'es', population: 200000},
    { id: 7, name: 'Oporto', country_code: 'pt', population: 231000},
    { id: 8, name: 'Guimaraes', country_code: 'pt', population: 150000},
    { id: 9, name: 'Lisboa', country_code: 'pt', population: 550000},
    { id: 10, name: 'Roma', country_code: 'it', population: 2800000},
    { id: 11, name: 'París', country_code: 'fr', population: 2300000},
];

exports.data = {countries, cities};