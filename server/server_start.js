'use strict';

const Hapi = require('@hapi/hapi');
const data = require('./initial_data').data;
console.log(`Datos iniciales: ${JSON.stringify(data, null, 2)}`);

const API_PREFIX = '/api'

function addCountryData(city) {
    let country = data.countries.find(c => c.code === city.country_code);
    return { ...city, country };
}

function addCity(newCity) {
    const maxId = data.cities.reduce((val, c) => val < c.id ? c.id : val, 0) + 1;
    newCity.id = maxId;
    let existsName = !!data.cities.find(c => c.name === newCity.name);
    if (existsName) {
        throw new Error(`Ya existe una ciudad llamada ${newCity.name}`);
    }
    let existsCountry = !!data.countries.find(c => c.code === newCity.country_code);
    if (!existsCountry) {
        throw new Error(`El país indicado no existe: ${newCity.country_code}`);
    }
    data.cities.push(newCity);
    return newCity;
}

function getCity(cityId) {
    let city = data.cities.find(c => c.id === cityId);
    if (!city) {
        throw new Error(`No existe ciudad con ID: ${cityId}`);
    }

    return addCountryData(city);
}

function deleteCity(cityId) {
    let cityIndex = data.cities.findIndex(c => c.id === cityId);
    if (cityIndex === -1) {
        throw new Error(`No existe ciudad con ID: ${cityId}`);
    }
    const deletedCity = data.cities.splice(cityIndex, 1)[0];
    return deletedCity;
}


function updateCity(newCityData) {
    let cityIndex = data.cities.findIndex(c => c.id === newCityData.id);
    if (cityIndex === -1) {
        throw new Error(`No existe ciudad con ID: ${cityId}`);
    }

    if (newCityData.country_code !== undefined) {
        let existsCountry = !!data.countries.find(c => c.code === newCityData.country_code);
        if (!existsCountry) {
            throw new Error(`El país indicado no existe: ${newCityData.country_code}`);
        }
    }
    let existsName = !!data.cities.find(c => c.name === newCityData.name && c.id != newCityData.id);
    if (existsName) {
        throw new Error(`Ya existe una ciudad llamada ${newCityData.name}`);
    }
    data.cities[cityIndex] = Object.assign(data.cities[cityIndex], newCityData);
    return addCountryData(data.cities[cityIndex]);
}

function returnError(h, error, status) {
    status = status || 500;
    return h.response({error}).code(status)
}

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost',
        routes: {
            cors: true
        }
    });

    server.route({
        method: 'GET',
        path: API_PREFIX,
        handler: (request, h) => 'Dummy server'
    });

    server.route({
        method: 'GET',
        path: `${API_PREFIX}/countries`,
        handler: (request, h) => data.countries.sort((c1, c2) => c1.name.localeCompare(c2.name))
    });

    server.route({
        method: 'GET',
        path: `${API_PREFIX}/cities`,
        handler: (request, h) => data.cities.sort((c1, c2) => c1.name.localeCompare(c2.name)).map(addCountryData)
    });

    server.route({
        method: 'POST',
        path: `${API_PREFIX}/cities`,
        handler: (request, h) => {
            try {
                return addCity(request.payload);
            } catch(e) {
                return returnError(h, e.toString());
            }
        }

    });

    server.route({
        method: 'GET',
        path: `${API_PREFIX}/cities/{id}`,
        handler: (request, h) => {
            try {
                return getCity(+request.params.id);
            } catch(e) {
                return returnError(h, e.toString(), 404);
            }
        }

    });

    server.route({
        method: 'DELETE',
        path: `${API_PREFIX}/cities/{id}`,
        handler: (request, h) => {
            try {
                return deleteCity(+request.params.id);
            } catch(e) {
                return returnError(h, e.toString(), 404);
            }
        }

    });

    server.route({
        method: 'POST',
        path: `${API_PREFIX}/cities/{id}`,
        handler: (request, h) => {
            try {
                const city = request.payload;
                city.id = +request.params.id;
                return updateCity(city);
            } catch(e) {
                return returnError(h, e.toString(), 404);
            }
        }

    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();
