# SfmDemoApp

Aplicación Angular creada con fines educativos donde se representan algunos de los elementos y conceptos habituales en las aplicaciones Angular

## Primeros pasos

Instalar [nodejs](https://nodejs.org/es/) (v 16.x) Node es el entorno de trabajo que utilizaremos para las herramientas como `@angular/cli`/`ng` que nos ayudarán en el desarrollo de las aplicaciones Angular. Junto con node también se instalará `npm` el gestor de paquetes que se encargará de instalar las dependencias de nuestro proyecto.

Instalar [`@angular/cli`](https://github.com/angular/angular-cli) 

    npm -g @angular/cli

Instalar [Visual Studio Code](https://code.visualstudio.com/) En el caso de que aun no lo tengamos en nuestro PC

Se recomienda añadir la extensión: [Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template) para trabajar con Angular

Clonamos el proyecto en nuestro PC con `git`:

    git clone https://gitlab.com/qo-oss/sfm-demo-app.git

Aunque también nos podemos bajar el proyecto en un archivo comprimido desde: [ZIP](https://gitlab.com/qo-oss/sfm-demo-app/-/archive/master/sfm-demo-app-master.zip)

Una vez con el proyecto en nuestro PC, lo abrimos con VSCode, para ello podemos hacerlo desde consola co el comando `code` o desde el propio VSCode con el comando: `Archivo > Abrir Carpeta...`

    code <path-a-proyecto>/sfm-demo-app

Desde el terminal del propio VSCode (Menú: `Ver > Terminal`) podemos lanzar los siguientes comandos:

Para instalar las dependencias del proyecto

    npm install 

Para lanzar el servidor REST de prueba que utilizaremos para invocar los servicios sobre paises y ciudades

    node server/server_start.js

Y finalmente para levantar el servidor web local para poder probar la aplicación, ejecutamos el siguiente comando en un nuevo terminal (el actual queda en ejecución con el servidor REST):

    npx ng serve

Si todo ha ido bien debemos tener la aplicación disponible en la url: http://localhost:4200/

