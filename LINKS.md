# Links de interés en Angular

Lista de links a páginas con información relevante acerca del framework Angular


https://angular.io/ Página oficial de Angular desde donde se puede acceder a documentación y recursos relacionados con el framework

https://angular.io/api API Completo de Angular donde podemos encontrar información relativa a las clases y componentes que vienen incluidos en el framework, por ejemplo [HttpClient](https://angular.io/api/common/http/HttpClient)

https://code.visualstudio.com/ IDE para desarrollar en Angular, podríamos utilizar cualqueir otro, pero VS Code es probablemente el más utilizado y uno de los mejores para Angular.

https://nodejs.org/es/ Entorno de trabajo de las herramientas de Angular, necesario para poder desarrollar aplicaciones Angular

https://www.npmjs.com/ Repositorio de paquetes node (npm), en este web podremos encontrar información de los paquetes de Angular y de todas las dependencias que eventualmente añadiremos a nuestro proyecto.

https://www.typescriptlang.org/ Página oficial de Typescript, el lenguaje de desarrollo en Angular, con posibilidad de probarlo online en el [Playground](https://www.typescriptlang.org/play)

https://material.angular.io/ Página oficial de Angular Material, el framework UI para aplicaciones Angular

https://www.primefaces.org/primeng/ PrimeNG, una alternativa a Angular Material

https://ng-bootstrap.github.io/#/home Ng-Boostrap, otra alternativa a Angular Material

https://github.com/angular/flex-layout/wiki Documentación sobre @angular/flex-layout La lib para organizar la estructura visual de los componentes Angular en los templates, la página con el API estático es especialmente interesante: [Static API](https://github.com/angular/flex-layout/wiki/Declarative-API-Overview)

https://material.angular.io/components/categories Componentes de Angular Material donde podemos encontrar ejemplos de uso de cada uno de los componentes del framework, por ejemplo: [Buttons examples](https://material.angular.io/components/button/examples)

https://fonts.google.com/icons?selected=Material+Icons+Outlined Iconos de Google, los utilizados por defecto por el componente `<mat-icon>` de Angular Material

## Otros links

https://runebook.dev/es/docs/angular/start Tutorial de Angular en español

http://es.html.net/tutorials/html/ Tutorial HTML en español, donde se explican los concpetos básicos del lenguaje de marcado, el tutorial no menciona Angular, aunque los conceptos de HTML son perfectamente aplicables a los templates de Angular

http://es.html.net/tutorials/css/ Tutorial CSS en español, al igual que el de HTML no se menciona a Angular, pero lo explicado aplica igualmente al CSS utilizado en los templates Angular.

https://softwarecrafters.io/typescript/typescript-tutorial-javascript-introduccion Tutorial de Typescript en español. Hay que tener en cuenta que se enfoca desde un punto de vista genérico del lenguaje, es decir, no como parte de una aplicación Angular, donde habría que extrapolar los conceptos explicados en el tutorial.