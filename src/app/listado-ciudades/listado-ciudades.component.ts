import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ConfirmDialogComponent, ConfirmDialogData } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { Ciudad } from '../domain/ciudad';
import { CiudadService } from '../services/ciudad.service';
import { MensajesService } from '../services/mensajes.service';

@Component({
  selector: 'app-listado-ciudades',
  templateUrl: './listado-ciudades.component.html',
  styleUrls: ['./listado-ciudades.component.scss']
})
export class ListadoCiudadesComponent implements OnInit {
  displayedColumns: string[] = ["id", "name", "population", "country", "actions"];
  ciudades: MatTableDataSource<Ciudad> = new MatTableDataSource();

  constructor(private router: Router,
    private mensajes: MensajesService,
    private dialog: MatDialog,
    private ciudadService: CiudadService) { }

  ngOnInit(): void {
    this.refresh();
  }

  private refresh() {
    this.ciudadService.getCities().subscribe((lista) => {
      this.ciudades = new MatTableDataSource<Ciudad>(lista);
    });
  }

  goToCreate() {
    this.router.navigateByUrl("/ciudad/new");
  }

  goToEdit(cityId: number) {
    this.router.navigateByUrl(`/ciudad/edit/${cityId}`);
  }

  deleteCity(city: Ciudad) {

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: <ConfirmDialogData>{
        titulo: "Borrar ciudad",
        mensaje: `¿Desea borrar la ciudad ${city.name}?`
      }
    });

    dialogRef.afterClosed().subscribe(confirm => {
      if (confirm) {
        this.ciudadService.deleteCity(city.id!).subscribe({
          next: (_) => {
            this.mensajes.showMsg(`Ciudad ${city.name} borrada con éxito`);
            this.refresh();
          },
          error: (respErr) => {
            this.mensajes.showMsg(`Error borrando la ciudad: ${respErr.error.error}`);
          }
        });
      }
    });


    
  }
}
