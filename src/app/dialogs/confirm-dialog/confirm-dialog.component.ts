import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ConfirmDialogData {
  titulo: string;
  mensaje: string;
}

@Component({
  selector: 'app-confirm-dialog',
  template: `<h1 mat-dialog-title>{{data.titulo}}</h1>
  <div mat-dialog-content>
    {{data.mensaje}}
  </div>
  <div fxLayout="row" fxLayoutGap="16px" style="margin-top: 16px;">

    <button mat-button color="primary" type="button" [mat-dialog-close]="true">Aceptar</button>
    <button mat-stroked-button type="button"  [mat-dialog-close]="false">Cancelar</button>
  </div>`
})
export class ConfirmDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogData) { }

  ngOnInit(): void {
    
  }

}
