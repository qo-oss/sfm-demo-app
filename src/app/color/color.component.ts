import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.scss']
})
export class ColorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  
  color : string | null = null;


  colorCambiado(newColor: string) : void {
    this.color = newColor;
  }
}
