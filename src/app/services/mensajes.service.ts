import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class MensajesService {

  constructor(    private _snackBar: MatSnackBar    ) { }

  showMsg(mensaje: string) {
    const sbRef = this._snackBar.open(mensaje, "CERRAR", {
      duration: 5000
    });
    sbRef.onAction().subscribe(() => {
      sbRef.dismiss();
    });
  }
}
