import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Ciudad } from '../domain/ciudad';
import { Country } from '../domain/country';

const PAISES : Country[] = [
  { code: 'es', name: 'España'},
  { code: 'fr', name: 'Francia'},
  { code: 'pt', name: 'Portugal'},
  { code: 'it', name: 'Italia'},
];

const PATH = environment.server_url;

@Injectable({
  providedIn: 'root'
})
export class CiudadService {

  constructor(private http: HttpClient) { }

  getCountryList() : Observable<Country[]> {
    return this.http.get<Country[]>(`${PATH}/countries`);
  }

  getCities() : Observable<Ciudad[]> {
    return this.http.get<Ciudad[]>(`${PATH}/cities`);
  }

  getCity(cityId: number) : Observable<Ciudad> {
    return this.http.get<Ciudad>(`${PATH}/cities/${cityId}`);
  }

  deleteCity(cityId: number) : Observable<Ciudad> {
    return this.http.delete<Ciudad>(`${PATH}/cities/${cityId}`);
  }

  updateCity(city: Ciudad) : Observable<Ciudad> {
    return this.http.post<Ciudad>(`${PATH}/cities/${city.id}`, city);
  }

  createCity(city: Ciudad) : Observable<Ciudad> {
    return this.http.post<Ciudad>(`${PATH}/cities`, city);
  }

}
