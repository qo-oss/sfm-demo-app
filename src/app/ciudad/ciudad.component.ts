import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Ciudad } from '../domain/ciudad';
import { Country } from '../domain/country';
import { CiudadService } from '../services/ciudad.service';
import { MensajesService } from '../services/mensajes.service';

@Component({
  selector: 'app-ciudad',
  templateUrl: './ciudad.component.html',
  styleUrls: ['./ciudad.component.scss']
})
export class CiudadComponent implements OnInit {

  form: FormGroup = this.fb.group({
    id: [null,],
    name: [null, Validators.required],
    population: [null, Validators.min(5000)],
    country_code: [null, Validators.required]
  });
  title?: string;
  isNew?: boolean;

  paises?: Country[];

  paises2?: Observable<Country[]>;

  constructor(private fb: FormBuilder,
    private mensajes: MensajesService,
    private route: ActivatedRoute,
    private router: Router,
    private serviceCiudad: CiudadService) { }

  ngOnInit(): void {
    this.serviceCiudad.getCountryList().subscribe((lista) => {
      this.paises = lista;
    });

    this.paises2 = this.serviceCiudad.getCountryList();

    this.route.params.subscribe(params => {
      const cityId: number = +params['id'];
      this.isNew = !cityId;
      if (this.isNew) {
        this.title = "Nueva ciudad";
      } else {
        this.title = "Modificar ciudad";
        this.serviceCiudad.getCity(cityId).subscribe(city => {
          this.form.patchValue(city, { emitEvent: false });
        });
      }
    });
  }



  guardar() {
    if (this.form.valid) {
      let ciudad: Ciudad = this.form.value;
      if (this.isNew) {
        this.serviceCiudad.createCity(ciudad).subscribe({
          next: (cityCreated) => {
            this.mensajes.showMsg(`Ciudad ${ciudad.name} creada con ID: ${cityCreated.id}`);
            this.router.navigateByUrl("/ciudad");
          },
          error: (respErr) => {
            this.mensajes.showMsg(`Error creando la ciudad: ${respErr.error.error}`);
          }
        });
      } else {
        this.serviceCiudad.updateCity(ciudad).subscribe({
          next: (cityModified) => {
            this.mensajes.showMsg(`Ciudad ${ciudad.name} modificada con éxito`);
            this.router.navigateByUrl("/ciudad");
          },
          error: (respErr) => {
            this.mensajes.showMsg(`Error modificando la ciudad: ${respErr.error.error}`);
          }
        });
      }
    } else {
      this.mensajes.showMsg(`Formularo no válido`);
    }
  }
}
