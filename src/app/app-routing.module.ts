import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CiudadComponent } from './ciudad/ciudad.component';
import { ColorComponent } from './color/color.component';
import { HomeComponent } from './home/home.component';
import { ListadoCiudadesComponent } from './listado-ciudades/listado-ciudades.component';

const routes: Routes = [
  { path: "", component: HomeComponent},
  { path: "color", component: ColorComponent },
  { path: "ciudad", children: [
    { path: "", component: ListadoCiudadesComponent },
    { path: "new", component: CiudadComponent },
    { path: "edit/:id", component: CiudadComponent },
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
