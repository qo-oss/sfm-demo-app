import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

const COLORES = ['orange', 'cyan', 'green', 'blue', 'red', 'magenta', 'yellow', '#aaaaaa'];

@Component({
  selector: 'color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.scss']
})
export class ColorPickerComponent implements OnInit {

  @Input()
  currentColor: string | null = null;

  @Output()
  changeColor: EventEmitter<string> = new EventEmitter<string>();

  colores = COLORES;
  constructor() { }

  ngOnInit(): void {
  }

  cambiaColor() {
    const idx = new Date().getTime() % COLORES.length;
    this.changeColor.emit(COLORES[idx]);
  }
  selectedColor(c: string) : string {
    return (c === this.currentColor ? 'black' : 'transparent')
  }
}
