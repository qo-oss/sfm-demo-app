export class Country {
    code: string;
    name: string;

    constructor(c: string, n: string) {
        this.code = c;
        this.name = n;
    }
}
