export class Ciudad {
    id?: number;
    name: string;
    country_code: string;
    population?: number;

    constructor(n: string, country: string, pop?: number, id?: number) {
        this.country_code = country;
        this.name = n;
        this.population = pop;
        this.id = id;
    }
}
