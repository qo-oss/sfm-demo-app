import { Component } from '@angular/core';



interface Ruta {
  link: string;
  label: string;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SFM Demo App';
  rutas : Ruta[] = [
    {link: "/", label: "Home"},
    {link: "/color", label: "Color"},
    {link: "/ciudad", label: "Ciudad"}
  ]
}
