# Curso Angular

El curso Angular que utiliza esta aplicación como referencia consta de 6 sesiones, divididas en 3 grupos de 2, un grupo por cada día de curso, el contenido de cada sesión se enumera a continuación:

## Sesión 1-1

* Aplicaciones web
* Dependencias para desarrollar en Angular
* Primeros pasos, creación de app desde cero
 
## Sesión 1-2
 
* Estructura componentes en Angular
* Directivas *ngIf / *ngFor
* Creación de componente desde cero
* Uso de @angular/flex-layout
* Parámetros de E/S (@Input / @Output)
 
## Sesión 2-1
 
* Routing
* Formularios reactivos
* Angular Material
 
## Sesión 2-2
 
* Angular Material en formulario
* Validaciones en formularios
* Creación de Service en Angular
* Uso básico Observable
 
## Sesión 3-1
 
* Datos del back con HttpClient y Observables
* Control de errores en llamada back
* Añadir proxy para llamar al back
* Routing con parámetros
* Personalización componente de alta y edición
 
## Sesión 3-2
 
* Diálogos en Angular Material
* Confirmación borrado mediante diálogo
* Responsive con flex-layout

